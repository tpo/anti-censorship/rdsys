package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

const (
	metricName = "rdsys_request_total{"
)

func main() {
	var urlsFile string
	var logFile string
	var cacheFile string
	var fetchUrlsSeconds int
	var publishSeconds int
	flag.StringVar(&urlsFile, "urls", "urls.txt", "File containing the urls of the metrics exporters, one url per line")
	flag.StringVar(&logFile, "log", "bridgedb-metrics.log", "File where the result metrics will be written")
	flag.StringVar(&cacheFile, "cache", "bridgedb-metrics.cache", "File where the cache will be kept, so data is not lost between restarts")
	flag.IntVar(&fetchUrlsSeconds, "fetch-seconds", 5*60, "The seconds between metrics fetches")
	flag.IntVar(&publishSeconds, "publish-seconds", 24*60*60, "The seconds between metrics publishing")
	flag.Parse()

	urls, err := readUrlsFile(urlsFile)
	if err != nil {
		log.Panic(err)
	}

	bm := newBridgedbMetrics()
	err = bm.load(cacheFile)
	if err != nil {
		log.Panic(err)
	}
	fetchUrls(urls, bm)

	for range time.Tick(time.Duration(fetchUrlsSeconds) * time.Second) {
		fetchUrls(urls, bm)

		if time.Now().Add(-time.Duration(publishSeconds) * time.Second).After(bm.LastPublished) {
			err := bm.publish(logFile, publishSeconds)
			if err != nil {
				log.Println("Can't access", logFile, "to write the metrics:", err)
			}
		}

		bm.save(cacheFile)
	}
}

func fetchUrls(urls []string, bm *bridgedbMetrics) {
	for _, url := range urls {
		data, err := fetchMetricData(url)
		if err != nil {
			log.Println(err)
			continue
		}

		bm.merge(data)
	}
}

func fetchMetricData(url string) (map[string]int, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("Error loading %s: %v", url, err)
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		return nil, fmt.Errorf("Error status for %s: %s", url, res.Status)
	}

	return parseMetrics(res.Body)
}

func readUrlsFile(urlsFile string) ([]string, error) {
	f, err := os.Open(urlsFile)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	urls := []string{}
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		urls = append(urls, scanner.Text())
	}
	return urls, nil
}
