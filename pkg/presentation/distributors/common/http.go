package common

import (
	"net"
	"net/http"
	"slices"
	"strings"
)

const (
	LastIP = iota
	FirstIP
)

func IpFromRequest(r *http.Request, trustProxy bool, position int) net.IP {
	var ip net.IP

	if trustProxy {
		header := r.Header.Get("X-Forwarded-For")
		forwarded := strings.Split(header, ",")

		if position == LastIP {
			slices.Reverse(forwarded)
		}
		for _, f := range forwarded {
			ip = net.ParseIP(strings.TrimSpace(f))
			if ip == nil {
				continue
			}
			if ip.IsLoopback() || ip.IsUnspecified() || ip.IsMulticast() || ip.IsLinkLocalUnicast() || ip.IsPrivate() {
				ip = nil
				continue
			}
			return ip
		}
	}

	// if no X-Forwarded-For header let's take the IP from the request
	ipStr := strings.Split(r.RemoteAddr, ":")[0]
	return net.ParseIP(ipStr)
}
