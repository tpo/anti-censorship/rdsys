// Copyright (c) 2024, The Tor Project, Inc.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package common

import (
	"encoding/json"

	"rsc.io/qr"
)

func QrCode(bridgeLines []string) ([]byte, error) {
	data, err := json.Marshal(bridgeLines)
	if err != nil {
		return nil, err
	}
	qrcode, err := qr.Encode(string(data), qr.M)
	if err != nil {
		return nil, err
	}
	return qrcode.PNG(), err
}
