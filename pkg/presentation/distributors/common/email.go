// Copyright (c) 2021-2024, The Tor Project, Inc.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package common

import (
	"bytes"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"log"
	"net/mail"
	"net/smtp"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	email "github.com/emersion/go-message/mail"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/ptutil/safeprom"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/internal"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/pkg/usecases/distributors"
)

var (
	emailCount = safeprom.NewCounterVecRegistered(prometheus.CounterOpts{
		Name: "email_processed_total",
		Help: "The total number of emails processed",
	},
		[]string{"status", "type"},
	)
	stoppedError = errors.New("Task was stopped")
)

const (
	durationIgnoreEmails = 24 * time.Hour
)

type SendFunction func(subject, body string, attachments ...*Attachment) error
type IncomingEmailHandler func(msg *mail.Message, send SendFunction) error

type Attachment struct {
	FileName    string
	ContentType string
	Data        []byte
}

type emailClient struct {
	cfg             *internal.EmailConfig
	imap            *client.Client
	dist            distributors.Distributor
	incomingHandler IncomingEmailHandler
	smtpAuth        *smtp.Auth
	fromAddress     *email.Address
}

func StartEmail(emailCfg *internal.EmailConfig, distCfg *internal.Config,
	dist distributors.Distributor, incomingHandler IncomingEmailHandler) {

	dist.Init(distCfg)
	e := emailClient{
		cfg:             emailCfg,
		dist:            dist,
		incomingHandler: incomingHandler,
	}
	if emailCfg.SmtpUsername != "" && emailCfg.SmtpPassword != "" {
		smtpHost := strings.Split(emailCfg.SmtpServer, ":")[0]
		smtpAuth := smtp.PlainAuth("", emailCfg.SmtpUsername, emailCfg.SmtpPassword, smtpHost)
		e.smtpAuth = &smtpAuth
	}
	var err error
	e.fromAddress, err = email.ParseAddress(emailCfg.Address)
	if err != nil {
		log.Panic("Wrong from address:", emailCfg.Address)
	}

	stop := make(chan struct{})
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT)
	signal.Notify(signalChan, syscall.SIGTERM)
	go func() {
		<-signalChan
		log.Printf("Caught SIGINT.")
		e.dist.Shutdown()
		close(stop)
	}()

listenLoop:
	for {
		select {
		case <-stop:
			break listenLoop
		default:
			var err error
			e.imap, err = initImap(emailCfg)
			if err != nil {
				log.Println("Can't init the imap client:", err)
				time.Sleep(time.Second)
				continue
			}

			if err := e.listenImapUpdates(stop); err != nil {
				log.Println("Error listening emails:", err)
				err = e.imap.Logout()
				if err != nil {
					log.Println("Error login out of imap:", err)
				}
			}
		}
	}
}

func initImap(emailCfg *internal.EmailConfig) (c *client.Client, err error) {
	splitedAddress := strings.Split(emailCfg.ImapServer, "://")
	if len(splitedAddress) != 2 {
		return nil, fmt.Errorf("Malformed imap server configuration: %s", emailCfg.ImapServer)
	}
	protocol := splitedAddress[0]
	serverAddress := splitedAddress[1]

	switch protocol {
	case "imaps":
		c, err = client.DialTLS(serverAddress, nil)
	case "imap":
		c, err = client.Dial(serverAddress)
	default:
		return nil, fmt.Errorf("Unkown protocol: %s", protocol)
	}
	if err != nil {
		return nil, err
	}

	err = c.Login(emailCfg.ImapUsername, emailCfg.ImapPassword)
	return c, err
}

func (e *emailClient) listenImapUpdates(stop <-chan struct{}) error {
	mbox, err := e.imap.Select("INBOX", false)
	if err != nil {
		return err
	}
	e.fetchMessages(mbox)

	for {
		update, err := e.waitForMailboxUpdate(stop)
		if err != nil {
			if errors.Is(err, stoppedError) {
				// don't propagate stopped error, we should just stop on this case
				return nil
			}
			return err
		}
		e.fetchMessages(update.Mailbox)
	}
}

func (e *emailClient) waitForMailboxUpdate(stop <-chan struct{}) (mboxUpdate *client.MailboxUpdate, err error) {
	// Create a channel to receive mailbox updates
	updates := make(chan client.Update, 1)
	e.imap.Updates = updates

	// Start idling
	done := make(chan error, 1)
	fetchStop := make(chan struct{})
	go func() {
		done <- e.imap.Idle(fetchStop, &client.IdleOptions{})
	}()

	// Listen for updates
waitLoop:
	for {
		select {
		case update := <-updates:
			var ok bool
			mboxUpdate, ok = update.(*client.MailboxUpdate)
			if ok {
				break waitLoop
			}
		case <-stop:
			close(fetchStop)
			return nil, stoppedError
		case err := <-done:
			return nil, err
		}
	}

	// We need to nil the updates channel or the client will hang on it
	// https://github.com/emersion/go-imap-idle/issues/16
	e.imap.Updates = nil
	close(fetchStop)
	<-done

	return mboxUpdate, nil
}

type flagUpdate struct {
	flag   string
	seqNum uint32
}

func (e *emailClient) fetchMessages(mboxStatus *imap.MailboxStatus) {
	criteria := imap.NewSearchCriteria()
	criteria.WithoutFlags = []string{imap.SeenFlag, imap.DeletedFlag}
	seqs, err := e.imap.Search(criteria)
	if err != nil {
		log.Println("Error getting unseen messages:", err)
		return
	}

	if len(seqs) == 0 {
		return
	}

	seqset := new(imap.SeqSet)
	seqset.AddNum(seqs...)
	items := []imap.FetchItem{imap.FetchItem("BODY.PEEK[]")}

	messages := make(chan *imap.Message, mboxStatus.Messages)
	flagUpdates := make(chan *flagUpdate, mboxStatus.Messages)
	go e.processIncomingMessages(messages, flagUpdates)

	log.Println("fetch", len(seqs), "messages from the imap server")
	err = e.imap.Fetch(seqset, items, messages)
	if err != nil {
		log.Println("Error fetching imap messages:", err)
	}

	for update := range flagUpdates {
		seqset := new(imap.SeqSet)
		seqset.AddNum(update.seqNum)

		item := imap.FormatFlagsOp(imap.AddFlags, true)
		flags := []interface{}{update.flag}
		err := e.imap.Store(seqset, item, flags, nil)
		if err != nil {
			log.Println("Error setting the delete flag", err)
		}
	}

	if err := e.imap.Expunge(nil); err != nil {
		log.Println("Error expunging messages from inbox", err)
	}
}

func (e *emailClient) processIncomingMessages(messages chan *imap.Message, flagUpdates chan *flagUpdate) {
	for msg := range messages {
		for _, literal := range msg.Body {
			email, err := mail.ReadMessage(literal)
			if err != nil {
				log.Println("Error parsing incoming email", err)
				emailCount.WithLabelValues("error", "parsing").Inc()
				continue
			}
			if dropEmail(email) {
				flagUpdates <- &flagUpdate{
					flag:   imap.DeletedFlag,
					seqNum: msg.SeqNum,
				}
				continue
			}

			send := func(subject, body string, attachments ...*Attachment) error {
				return e.reply(email, subject, body, attachments)
			}

			err = e.incomingHandler(email, send)
			if err != nil {
				log.Println("Error handling incoming email ", email.Header.Get("Message-ID"), ":", err)
				emailCount.WithLabelValues("error", "handling").Inc()

				date, err := email.Header.Date()
				if err != nil || date.Add(durationIgnoreEmails).Before(time.Now()) {
					log.Println("Give up with the email, marked as readed so it will not be processed anymore")
					flagUpdates <- &flagUpdate{
						flag:   imap.SeenFlag,
						seqNum: msg.SeqNum,
					}
				}
			} else {
				// delete the email as it was fully processed
				flagUpdates <- &flagUpdate{
					flag:   imap.DeletedFlag,
					seqNum: msg.SeqNum,
				}
				emailCount.WithLabelValues("success", "handling").Inc()
			}
		}
	}
	close(flagUpdates)
}

func dropEmail(msg *mail.Message) bool {
	// automatic responses RFC 3834
	if header := msg.Header.Get("Auto-Submitted"); header != "" && header != "no" {
		log.Println("Drop autogenerated email:", msg.Header.Get("Message-ID"))
		emailCount.WithLabelValues("drop", "auto-submitted").Inc()
		return true
	}

	// reports of Mail System Administrative Messages RFC 3462
	if header := msg.Header.Get("Content-Type"); strings.Contains(header, "multipart/report") {
		log.Println("Drop report email:", msg.Header.Get("Message-ID"))
		emailCount.WithLabelValues("drop", "report").Inc()
		return true
	}
	return false
}

func (e *emailClient) reply(originalMessage *mail.Message, subject, body string, attachments []*Attachment) error {
	sender, err := originalMessage.Header.AddressList("From")
	if err != nil {
		return err
	}
	if len(sender) != 1 {
		return fmt.Errorf("Unexpected email from: %s", originalMessage.Header.Get("From"))
	}

	var headers email.Header
	headers.SetAddressList("From", []*email.Address{e.fromAddress})
	headers.SetAddressList("To", sender)
	headers.SetDate(time.Now())
	headers.SetSubject(subject)
	headers.SetMsgIDList("In-Reply-To", []string{originalMessage.Header.Get("Message-ID")})
	headers.Set("Auto-Submitted", "auto-replied")

	var mailBuff bytes.Buffer
	mailWriter, err := email.CreateWriter(&mailBuff, headers)
	if err != nil {
		return err
	}
	defer mailWriter.Close()

	var partHeaders email.InlineHeader
	partHeaders.Set("MIME-version", "1.0")
	partHeaders.Set("Content-Type", "text/plain; charset=\"utf-8\"")

	inline, err := mailWriter.CreateInline()
	if err != nil {
		return err
	}
	defer inline.Close()
	part, err := inline.CreatePart(partHeaders)
	if err != nil {
		return err
	}
	defer part.Close()

	_, err = io.WriteString(part, body)
	if err != nil {
		return err
	}

	for _, attachment := range attachments {
		var aHeaders email.AttachmentHeader
		aHeaders.Set("Content-Type", attachment.ContentType)
		aHeaders.SetFilename(attachment.FileName)

		w, err := mailWriter.CreateAttachment(aHeaders)
		if err != nil {
			return err
		}
		defer w.Close()

		w.Write(attachment.Data)
	}

	return e.send(sender[0].Address, &mailBuff)
}

func (e *emailClient) send(to string, msg io.Reader) error {
	c, err := smtp.Dial(e.cfg.SmtpServer)
	if err != nil {
		return err
	}

	if e.smtpAuth != nil {
		tlsConfig := tls.Config{
			ServerName: strings.Split(e.cfg.SmtpServer, ":")[0],
		}
		if err := c.StartTLS(&tlsConfig); err != nil {
			return err
		}
		if err := c.Auth(*e.smtpAuth); err != nil {
			return err
		}
	}

	if err := c.Mail(e.cfg.Address); err != nil {
		return err
	}
	if err := c.Rcpt(to); err != nil {
		return err
	}

	wc, err := c.Data()
	if err != nil {
		return err
	}
	io.Copy(wc, msg)
	err = wc.Close()
	if err != nil {
		return err
	}
	return c.Quit()
}
