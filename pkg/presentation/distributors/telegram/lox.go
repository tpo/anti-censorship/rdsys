// Copyright (c) 2024, The Tor Project, Inc.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package telegram

import (
	"encoding/json"
	"fmt"

	"github.com/nicksnyder/go-i18n/v2/i18n"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/pkg/usecases/distributors/telegram"
	tb "gopkg.in/telebot.v3"
)

func (t *TBot) getLoxHelp(c tb.Context) error {
	const helpmsg = "Lox *(alpha)* is not quite ready yet, but will be available soon!"

	localizer, menu := t.newLocalizer(c)
	msg, _ := localizer.Localize(&i18n.LocalizeConfig{
		DefaultMessage: &i18n.Message{
			ID:    "TelegramLoxHelp",
			Other: helpmsg,
		},
	})
	return c.Send(msg, menu)
}

func (t *TBot) getLoxInvitation(c tb.Context) error {
	localizer, _ := t.newLocalizer(c)
	if c.Sender().IsBot {
		msg, _ := localizer.Localize(&i18n.LocalizeConfig{
			DefaultMessage: &i18n.Message{
				ID:    "TelegramNoInvitation",
				Other: "No invitation for bots, sorry",
			},
		})
		return c.Send(msg)
	}
	userID := c.Sender().ID
	invitation, err := t.dist.GetInvitation(userID)
	if err != nil {
		var error_msg string
		switch x := err.(type) {
		case *telegram.IdFreshnessError:
			error_msg, _ = localizer.Localize(&i18n.LocalizeConfig{
				DefaultMessage: &i18n.Message{
					ID:    "IdFreshnessError",
					Other: "Your account is too new, an invitation can not be issued.",
				},
			})
		case *telegram.InvitationLimitError:
			localized_err, _ := localizer.Localize(&i18n.LocalizeConfig{
				DefaultMessage: &i18n.Message{
					ID:    "InvitationLimitError",
					Other: "You have already requested an invite, you can request again on %s",
				},
			})
			error_msg = fmt.Sprintf(localized_err, x.Error())
		case *telegram.LoxRequestError:
			error_msg, _ = localizer.Localize(&i18n.LocalizeConfig{
				DefaultMessage: &i18n.Message{
					ID:    "LoxRequestError",
					Other: "There was a problem making the invite request. Try again in a while",
				},
			})
		default:
			error_msg, _ = localizer.Localize(&i18n.LocalizeConfig{
				DefaultMessage: &i18n.Message{
					ID:    "LoxErrorMessage",
					Other: "Unknown Lox Error, please try again",
				},
			})
		}
		t.bot.Send(c.Sender(), error_msg, tb.ModeMarkdown)
		return nil
	}
	msg, _ := localizer.Localize(&i18n.LocalizeConfig{
		DefaultMessage: &i18n.Message{
			ID:  "LoxInvitation",
			One: "***Your Lox Invitation:***",
		},
	})
	t.bot.Send(c.Sender(), msg, tb.ModeMarkdown)
	var v map[string]string
	err = json.Unmarshal(invitation, &v)
	if err != nil {
		error_msg, _ := localizer.Localize(&i18n.LocalizeConfig{
			DefaultMessage: &i18n.Message{
				ID:    "LoxErrorMessage",
				Other: "Unknown Lox Error, please try again",
			},
		})
		t.bot.Send(c.Sender(), error_msg, tb.ModeMarkdown)
	}
	response := string(v["invite"])
	t.bot.Send(c.Sender(), response, tb.ModeMarkdown)

	return nil
}
