// Copyright (c) 2021-2024, The Tor Project, Inc.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package core

import (
	"log"
	"slices"
	"sync"
	"time"
)

const (
	// These constants represent resource event types.  The backend informs
	// distributors if a resource is new, has changed, or has disappeared.
	ResourceUnchanged = iota
	ResourceIsNew
	ResourceChanged
	ResourceIsGone
	ResourceError
)

// BackendResources implements a collection of resources for our backend.  The
// backend uses this data structure to keep track of all of its resource types.
type BackendResources struct {
	Collection

	// OnlyFunctional resources will be provided to distributors
	OnlyFunctional bool

	// UseBandwidthRatio to decide wich bridges to distribute
	UseBandwidthRatio bool

	// The mutex us used to protect the access to ResourceDiffRecipients.
	// The hashrings in the Collection have their own mutex and the entries
	// of the Collection map are only set during intialization.
	sync.RWMutex
	// ResourceDiffRecipients maps a distributor name (e.g., "moat") to an resource
	// diff recipient struct that helps us keep track of notifying distributors when
	// their resources change. It is used by the resource stream API.
	ResourceDiffRecipients map[string][]*ResourceDiffRecipient
}

// ResourceDiffRecipient represents the recipient of a resource event, i.e. a
// distributor; or rather, what we need to send updates to said distributor.
type ResourceDiffRecipient struct {
	Chan    chan *ResourceDiff
	Request *ResourceRequest
}

// NewBackendResources creates and returns a new resource collection.
// rTypes is a map of resource type names to a boolean indicating if the resource is unpartitioend
func NewBackendResources(cfg *CollectionConfig) *BackendResources {
	r := &BackendResources{}
	r.Collection = NewCollection(cfg)
	for _, rc := range cfg.Types {
		if !rc.Unpartitioned {
			r.Collection[rc.Type] = newPartitionedWithDistributors(r.Collection[rc.Type])
		}
	}
	r.ResourceDiffRecipients = make(map[string][]*ResourceDiffRecipient)
	return r
}

// Add adds the given resource to the resource collection.  If the resource
// already exists but has changed (i.e. its unique ID remains the same but its
// object ID changed), we update the existing resource.
func (ctx *BackendResources) Add(r1 Resource) {
	hashring, exists := ctx.Collection[r1.Type()]
	if !exists {
		return
	}

	event := hashring.AddOrUpdate(r1)
	if event != ResourceUnchanged {
		ctx.propagateUpdate(r1, event)
	}
}

// Prune removes expired resources and notifies on not working ones
func (ctx *BackendResources) Prune(rName string) []Resource {

	hashring := ctx.Collection[rName]
	prunedResources := hashring.Prune()
	for _, resource := range hashring.GetAll() {
		if !ctx.isResourceWorking(resource) {
			prunedResources = append(prunedResources, resource)
		}
	}
	for _, resource := range prunedResources {
		ctx.propagateUpdate(resource, ResourceIsGone)
	}
	return prunedResources
}

// propagateUpdate sends updates about new, changed, and gone resources to
// channels, allowing the backend to immediately inform a distributor of the
// update.
func (ctx *BackendResources) propagateUpdate(r Resource, event int) {
	ctx.RLock()
	defer ctx.RUnlock()

	if _, exists := ctx.Collection[r.Type()]; !exists {
		return
	}

	// Prepare the hashring difference that we're about to send.
	diff := &ResourceDiff{}
	rm := ResourceMap{r.Type(): []Resource{r}}
	switch event {
	case ResourceIsNew:
		diff.New = rm
	case ResourceChanged:
		diff.Changed = rm
	case ResourceIsGone:
		diff.Gone = rm
	default:
		return
	}

	distName, err := ctx.Collection[r.Type()].getPartitionName(r)
	if err != nil {
		return
	}

	// if the resource is unpartitioned we need to find what distributors to send the diff
	if distName == "" {
		for _, eventRecipients := range ctx.ResourceDiffRecipients {
			for _, eventRecipient := range eventRecipients {
				if eventRecipient.Request.HasResourceType(r.Type()) {
					eventRecipient.Chan <- diff
				}
			}
		}
		return
	}

	for _, eventRecipient := range ctx.ResourceDiffRecipients[distName] {
		if !eventRecipient.Request.HasResourceType(r.Type()) || !eventRecipient.Request.HasResourceIP(r.IPVersion()) {
			continue
		}
		eventRecipient.Chan <- diff
	}
}

// RegisterChan registers a channel to be informed about resource updates.
func (ctx *BackendResources) RegisterChan(req *ResourceRequest, recipient chan *ResourceDiff) {
	ctx.Lock()
	defer ctx.Unlock()

	distName := req.RequestOrigin
	log.Printf("Registered new channel for distributor %q to receive updates.", distName)

	er := &ResourceDiffRecipient{Request: req, Chan: recipient}
	_, exists := ctx.ResourceDiffRecipients[distName]
	if !exists {
		ctx.ResourceDiffRecipients[distName] = []*ResourceDiffRecipient{er}
	} else {
		ctx.ResourceDiffRecipients[distName] = append(ctx.ResourceDiffRecipients[distName], er)
	}
}

// UnregisterChan unregisters a channel to be informed about resource updates.
func (ctx *BackendResources) UnregisterChan(distName string, recipient chan *ResourceDiff) {
	ctx.Lock()
	defer ctx.Unlock()

	ctx.ResourceDiffRecipients[distName] = slices.DeleteFunc(ctx.ResourceDiffRecipients[distName], func(er *ResourceDiffRecipient) bool {
		return er.Chan == recipient
	})
}

// Get returns a struct that contains the state of resources
// distributor.
func (ctx *BackendResources) Get(distName string, rType string, ipVersion IPVersion) ResourceState {
	hashring := ctx.GetHashring(distName, rType)
	if hashring == nil {
		log.Printf("Failed to get resources for distributor %q", distName)
		return ResourceState{}
	}

	var resourceState = ResourceState{}
	for _, resource := range hashring.GetAll() {
		if !ipVersion.ValidFor(resource.IPVersion()) {
			continue
		}
		if ctx.isResourceWorking(resource) {
			resourceState.Working = append(resourceState.Working, resource)
		} else {
			resourceState.Notworking = append(resourceState.Notworking, resource)
		}
	}
	return resourceState
}

func (ctx *BackendResources) isResourceWorking(resource Resource) bool {
	rTest := resource.TestResult()
	//If the context includes not-functional bridges or the resource's state is functional
	//AND if the context doesn't use the bandwidth ratio or the speed is not speed rejected
	if (!ctx.OnlyFunctional || rTest.State == StateFunctional) && (!ctx.UseBandwidthRatio || rTest.Speed != SpeedRejected) {
		// If the resource is set as a working resource, update the Last working time to now
		// https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/209
		rTest.LastWorking = time.Now().UTC()
		return true
	}

	return false
}

type partitionedWithDistributors struct {
	*partitionedHashring
}

func newPartitionedWithDistributors(rg ResourceGroup) *partitionedWithDistributors {
	p := rg.(*partitionedHashring)
	p.partitions["none"] = NewHashring()
	return &partitionedWithDistributors{p}
}

func (p partitionedWithDistributors) Add(resource Resource) error {
	name, err := p.getPartitionName(resource)
	if err != nil {
		return err
	}
	p.addRelationIdentifiers(resource, name)
	hashring := p.partitions[name]
	return hashring.Add(resource)
}

func (p partitionedWithDistributors) AddOrUpdate(resource Resource) int {
	name, err := p.getPartitionName(resource)
	if err != nil {
		log.Println("Error updating partitioned distributor:", err)
		return ResourceError
	}
	p.addRelationIdentifiers(resource, name)
	hashring := p.partitions[name]
	return hashring.AddOrUpdate(resource)
}

func (p partitionedWithDistributors) Remove(resource Resource) error {
	name, err := p.getPartitionName(resource)
	if err != nil {
		return err
	}
	hashring := p.partitions[name]
	return hashring.Remove(resource)
}

func (p partitionedWithDistributors) getPartitionName(resource Resource) (string, error) {
	distName := resource.Distributor()
	if distName != "" {
		if _, ok := p.partitions[distName]; !ok {
			distName = "none"
		}
		return distName, nil
	}
	return p.partitionedHashring.getPartitionName(resource)
}

func (p partitionedWithDistributors) addRelationIdentifiers(resource Resource, partitionName string) {
	if partitionName == "none" {
		return
	}
	p.partitionedHashring.addRelationIdentifiers(resource, partitionName)
}
