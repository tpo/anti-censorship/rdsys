// Copyright (c) 2021-2024, The Tor Project, Inc.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package core

import (
	"testing"
	"time"
)

var (
	partitionName    = "partition"
	proportions      = map[string]int{partitionName: 1}
	collectionConfig = CollectionConfig{
		Types: []TypeConfig{
			{Type: "dummy", Proportions: proportions},
		},
	}
)

func TestAddCollection(t *testing.T) {
	d1 := NewDummy(1, 1)
	d2 := NewDummy(2, 2)
	d3 := NewDummy(3, 2)
	c := NewBackendResources(&collectionConfig)

	c.Add(d1)
	if c.Collection[d1.Type()].Len() != 1 {
		t.Errorf("expected length 1 but got %d", len(c.Collection))
	}
	c.Add(d2)
	if c.Collection[d1.Type()].Len() != 2 {
		t.Errorf("expected length 2 but got %d", len(c.Collection))
	}
	// d3 has the same unique ID as d2 but a different object ID.  Our
	// collection should update d2 but not create a new element.
	c.Add(d3)
	if c.Collection[d1.Type()].Len() != 2 {
		t.Errorf("expected length 2 but got %d", len(c.Collection))
	}

	hashring := c.GetHashring(partitionName, d3.Type())
	elems, err := hashring.GetMany(Hashkey(0), 2, &BridgeRequest{})
	if err != nil {
		t.Errorf(err.Error())
	}
	if elems[0] != d1 {
		t.Errorf("got unexpected element")
	}
	if elems[1] != d3 {
		t.Errorf("got unexpected element: %d", elems[1].Oid())
	}
}

func TestStringCollection(t *testing.T) {
	c := NewBackendResources(&collectionConfig)
	s := c.String()
	expected := "0 dummy"
	if s != expected {
		t.Errorf("expected %q but got %q", expected, s)
	}
}

func TestPruneCollection(t *testing.T) {
	d := NewDummy(1, 1)
	d.ExpiryTime = time.Minute * 10
	c := NewBackendResources(&collectionConfig)
	c.Add(d)
	hLength := func() int { return c.Collection[d.Type()].Len() }

	// We should now have one element in the hashring.
	if hLength() != 1 {
		t.Fatalf("expectec hashring of length 1 but got %d", hLength())
	}

	// Expire the hashring node.
	hashring := c.GetHashring(partitionName, d.Type())
	i, err := hashring.getIndex(d.Uid())
	if err != nil {
		t.Errorf("failed to retrieve existing resource: %s", err)
	}
	node := hashring.hashnodes[i]
	node.lastUpdate = time.Now().UTC().Add(-d.ExpiryTime - time.Minute)

	for rName := range c.Collection {
		pruned := c.Prune(rName)
		if len(pruned) != 1 {
			t.Fatal("Unexpected length of pruned resources:", pruned)
		}
		if pruned[0].Oid() != d.Oid() {
			t.Error("Unexpected pruned resource:", pruned[0])
		}
	}
	// Pruning should have left our hashring empty.
	if hLength() != 0 {
		t.Fatalf("expectec hashring of length 0 but got %d", hLength())
	}
}

func TestPruneNotWorking(t *testing.T) {
	d := NewDummy(1, 1)
	d.TestResult().State = StateDysfunctional
	c := NewBackendResources(&collectionConfig)
	c.OnlyFunctional = true
	c.Add(d)
	hLength := func() int { return c.Collection[d.Type()].Len() }

	// We should now have one element in the hashring.
	if hLength() != 1 {
		t.Fatalf("expectec hashring of length 1 but got %d", hLength())
	}

	for rName := range c.Collection {
		pruned := c.Prune(rName)
		if len(pruned) != 1 {
			t.Fatal("Unexpected length of pruned resources:", pruned)
		}
		if pruned[0].Oid() != d.Oid() {
			t.Error("Unexpected pruned resource:", pruned[0])
		}
	}

	if hLength() != 1 {
		t.Fatalf("Pruning a non working device should not have removed it from the hashring, but got %d", hLength())
	}
}

func TestCollectionProportions(t *testing.T) {
	distName := "distributor"
	d := NewDummy(1, 1)

	c1 := NewBackendResources(&collectionConfig)
	c1.Add(d)
	resources := c1.Get(distName, d.Type(), IPAny)
	if len(resources.Working) != 0 {
		t.Errorf("Unexpected resource len %d: %v", len(resources.Working), resources)
	}

	c2 := NewBackendResources(&CollectionConfig{
		Types: []TypeConfig{
			{Type: d.Type(), Proportions: map[string]int{distName: 1}},
		},
	})
	c2.Add(d)
	resources = c2.Get(distName, d.Type(), IPAny)
	if len(resources.Working) != 1 {
		t.Fatalf("Unexpected resource len %d: %v", len(resources.Working), resources)
	}
	if resources.Working[0].Oid() != d.Oid() {
		t.Errorf("Unexpected dummy resource: %v", resources.Working[0])
	}
}

func TestUnpartitioned(t *testing.T) {
	d1 := NewDummy(1, 1)
	d2 := NewDummy(2, 2)
	d3 := NewDummy(3, 2)
	c := NewBackendResources(&CollectionConfig{
		Types: []TypeConfig{
			{Type: d1.Type(), Unpartitioned: true},
		},
	})

	c.Add(d1)
	if c.Collection[d1.Type()].Len() != 1 {
		t.Errorf("expected length 1 but got %d", len(c.Collection))
	}
	c.Add(d2)
	if c.Collection[d1.Type()].Len() != 2 {
		t.Errorf("expected length 2 but got %d", len(c.Collection))
	}
	// d3 has the same unique ID as d2 but a different object ID.  Our
	// collection should update d2 but not create a new element.
	c.Add(d3)
	if c.Collection[d1.Type()].Len() != 2 {
		t.Errorf("expected length 2 but got %d", len(c.Collection))
	}

	hashring := c.GetHashring("", d3.Type())
	elems, err := hashring.GetMany(Hashkey(0), 2, &BridgeRequest{})
	if err != nil {
		t.Errorf(err.Error())
	}
	if elems[0] != d1 {
		t.Errorf("got unexpected element")
	}
	if elems[1] != d3 {
		t.Errorf("got unexpected element: %d", elems[1].Oid())
	}

	hashring = c.GetHashring("the name should not matter", d3.Type())
	if hashring.Len() != 2 {
		t.Errorf("Didn't get the right number of unpartitioned resources: %d", hashring.Len())
	}
}

func TestNoneDistributor(t *testing.T) {
	dnone := NewDummy(1, 1)
	dnone.Distribution = "none"
	dnone.RelationIds = []string{"fingerprint"}
	dany := NewDummy(2, 2)
	dany.RelationIds = []string{"fingerprint"}
	dany2 := NewDummy(3, 3)
	dany2.RelationIds = []string{"fingerprint"}

	c := NewBackendResources(&collectionConfig)
	c.Add(dnone)
	c.Add(dany)
	c.Add(dany2)

	hashring := c.GetHashring(partitionName, dany.Type())
	if hashring.Len() != 2 {
		t.Errorf("Didn't get the right number of resources: %d", hashring.Len())
	}
	elems, err := hashring.GetMany(Hashkey(0), 2, &BridgeRequest{})
	if err != nil {
		t.Errorf(err.Error())
	}
	if elems[0] != dany {
		t.Errorf("got unexpected element")
	}
	if elems[1] != dany2 {
		t.Errorf("got unexpected element: %d", elems[1].Oid())
	}

	hashring = c.GetHashring("none", dany.Type())
	if hashring.Len() != 1 {
		t.Errorf("Didn't get the right number of resources: %d", hashring.Len())
	}
	elems, err = hashring.GetMany(Hashkey(0), 1, &BridgeRequest{})
	if err != nil {
		t.Errorf(err.Error())
	}
	if elems[0] != dnone {
		t.Errorf("got unexpected element")
	}
}

func TestUnknownDistributor(t *testing.T) {
	dnone := NewDummy(1, 1)
	dnone.Distribution = "unrecognized"
	dnone.RelationIds = []string{"fingerprint"}
	dany := NewDummy(2, 2)
	dany.RelationIds = []string{"fingerprint"}
	dany2 := NewDummy(3, 3)
	dany2.RelationIds = []string{"fingerprint"}

	c := NewBackendResources(&collectionConfig)
	c.Add(dnone)
	c.Add(dany)
	c.Add(dany2)

	hashring := c.GetHashring(partitionName, dany.Type())
	if hashring.Len() != 2 {
		t.Errorf("Didn't get the right number of resources: %d", hashring.Len())
	}
	elems, err := hashring.GetMany(Hashkey(0), 2, &BridgeRequest{})
	if err != nil {
		t.Errorf(err.Error())
	}
	if elems[0] != dany {
		t.Errorf("got unexpected element")
	}
	if elems[1] != dany2 {
		t.Errorf("got unexpected element: %d", elems[1].Oid())
	}

	hashring = c.GetHashring("none", dany.Type())
	if hashring.Len() != 1 {
		t.Errorf("Didn't get the right number of resources: %d", hashring.Len())
	}
	elems, err = hashring.GetMany(Hashkey(0), 1, &BridgeRequest{})
	if err != nil {
		t.Errorf(err.Error())
	}
	if elems[0] != dnone {
		t.Errorf("got unexpected element")
	}
}

func TestRegisterAdd(t *testing.T) {
	d1 := NewDummy(1, 1)
	d4 := NewDummy(2, 2)
	d4.IPVer = IPv4
	d6 := NewDummy(3, 3)
	d6.IPVer = IPv6

	c := NewBackendResources(&collectionConfig)

	chans := map[IPVersion]chan *ResourceDiff{}
	chans[IPAny] = make(chan *ResourceDiff, 1)
	chans[IPv4] = make(chan *ResourceDiff, 1)
	chans[IPv6] = make(chan *ResourceDiff, 1)
	for ipver, ch := range chans {
		c.RegisterChan(&ResourceRequest{
			RequestOrigin: partitionName,
			ResourceTypes: []string{d1.Type()},
			IPVersion:     ipver,
		}, ch)
	}

	c.Add(d1)
	for ipver, ch := range chans {
		if len(ch) != 1 {
			t.Error("Update expected after add, ipversion:", ipver)
			continue
		}

		diff := <-ch
		if diff.FullUpdate {
			t.Error("Not expecting full update, ipversion:", ipver, "diff:", diff)
		}
		if len(diff.New[d1.Type()]) != 1 {
			t.Fatal("Not resource in new diff on add, ipversion:", ipver, "diff:", diff)
		}
		if diff.New[d1.Type()][0] != d1 {
			t.Error("Unexpected resource for ipversion:", ipver, "resource:", diff.New[d1.Type()][0])
		}
		if len(diff.Changed[d1.Type()]) != 0 {
			t.Error("Not empty changed diff on add, ipversion:", ipver, "diff:", diff)
		}
		if len(diff.Gone[d1.Type()]) != 0 {
			t.Error("Not empty gone diff on add, ipversion:", ipver, "diff:", diff)
		}
	}
	c.Add(d4)
	for ipver, ch := range chans {
		if ipver == IPv6 {
			if len(ch) != 0 {
				t.Error("No update expected for IPv6 with IPv4 resource")
				<-ch
			}
			continue
		}
		if len(ch) != 1 {
			t.Error("Update expected after add, ipversion:", ipver)
			continue
		}

		diff := <-ch
		if len(diff.New[d1.Type()]) != 1 {
			t.Fatal("Not resource in new diff on add, ipversion:", ipver, "diff:", diff)
		}
		if diff.New[d1.Type()][0] != d4 {
			t.Error("Unexpected resource for ipversion:", ipver, "resource:", diff.New[d1.Type()][0])
		}
		if len(diff.Changed[d1.Type()]) != 0 {
			t.Error("Not empty changed diff on add, ipversion:", ipver, "diff:", diff)
		}
		if len(diff.Gone[d1.Type()]) != 0 {
			t.Error("Not empty gone diff on add, ipversion:", ipver, "diff:", diff)
		}
	}
	c.Add(d6)
	for ipver, ch := range chans {
		if ipver == IPv4 {
			if len(ch) != 0 {
				t.Error("No update expected for IPv4 with IPv6 resource")
				<-ch
			}
			continue
		}
		if len(ch) != 1 {
			t.Error("Update expected after add, ipversion:", ipver)
			continue
		}

		diff := <-ch
		if len(diff.New[d1.Type()]) != 1 {
			t.Fatal("Not resource in new diff on add, ipversion:", ipver, "diff:", diff)
		}
		if diff.New[d1.Type()][0] != d6 {
			t.Error("Unexpected resource for ipversion:", ipver, "resource:", diff.New[d1.Type()][0])
		}
		if len(diff.Changed[d1.Type()]) != 0 {
			t.Error("Not empty changed diff on add, ipversion:", ipver, "diff:", diff)
		}
		if len(diff.Gone[d1.Type()]) != 0 {
			t.Error("Not empty gone diff on add, ipversion:", ipver, "diff:", diff)
		}
	}
}

func TestRegisterUpdate(t *testing.T) {
	d1 := NewDummy(1, 1)
	d4 := NewDummy(2, 2)
	d4.IPVer = IPv4
	d6 := NewDummy(3, 3)
	d6.IPVer = IPv6

	c := NewBackendResources(&collectionConfig)
	c.Add(d1)
	c.Add(d4)
	c.Add(d6)

	chans := map[IPVersion]chan *ResourceDiff{}
	chans[IPAny] = make(chan *ResourceDiff, 1)
	chans[IPv4] = make(chan *ResourceDiff, 1)
	chans[IPv6] = make(chan *ResourceDiff, 1)
	for ipver, ch := range chans {
		c.RegisterChan(&ResourceRequest{
			RequestOrigin: partitionName,
			ResourceTypes: []string{d1.Type()},
			IPVersion:     ipver,
		}, ch)
	}

	d1update := NewDummy(10, d1.Uid())
	c.Add(d1update)
	for ipver, ch := range chans {
		if len(ch) != 1 {
			t.Error("Update expected after add, ipversion:", ipver)
			continue
		}

		diff := <-ch
		if diff.FullUpdate {
			t.Error("Not expecting full update, ipversion:", ipver, "diff:", diff)
		}
		if len(diff.Changed[d1.Type()]) != 1 {
			t.Fatal("Not resource in changed diff on update, ipversion:", ipver, "diff:", diff)
		}
		if diff.Changed[d1.Type()][0] != d1update {
			t.Error("Unexpected resource for ipversion:", ipver, "resource:", diff.New[d1.Type()][0])
		}
		if len(diff.New[d1.Type()]) != 0 {
			t.Error("Not empty new diff on add, ipversion:", ipver, "diff:", diff)
		}
		if len(diff.Gone[d1.Type()]) != 0 {
			t.Error("Not empty gone diff on add, ipversion:", ipver, "diff:", diff)
		}
	}

	d4update := NewDummy(10, d4.Uid())
	d4update.IPVer = IPv4
	c.Add(d4update)
	for ipver, ch := range chans {
		if ipver == IPv6 {
			if len(ch) != 0 {
				t.Error("No update expected for IPv6 with IPv4 resource")
				<-ch
			}
			continue
		}
		if len(ch) != 1 {
			t.Error("Update expected after add, ipversion:", ipver)
			continue
		}

		diff := <-ch
		if len(diff.Changed[d1.Type()]) != 1 {
			t.Fatal("Not resource in changed diff on add, ipversion:", ipver, "diff:", diff)
		}
		if diff.Changed[d1.Type()][0] != d4update {
			t.Error("Unexpected resource for ipversion:", ipver, "resource:", diff.New[d1.Type()][0])
		}
		if len(diff.New[d1.Type()]) != 0 {
			t.Error("Not empty new diff on add, ipversion:", ipver, "diff:", diff)
		}
		if len(diff.Gone[d1.Type()]) != 0 {
			t.Error("Not empty gone diff on add, ipversion:", ipver, "diff:", diff)
		}
	}

	d6update := NewDummy(10, d6.Uid())
	d6update.IPVer = IPv6
	c.Add(d6update)
	for ipver, ch := range chans {
		if ipver == IPv4 {
			if len(ch) != 0 {
				t.Error("No update expected for IPv4 with IPv6 resource")
				<-ch
			}
			continue
		}
		if len(ch) != 1 {
			t.Error("Update expected after add, ipversion:", ipver)
			continue
		}

		diff := <-ch
		if len(diff.Changed[d1.Type()]) != 1 {
			t.Fatal("Not resource in changed diff on add, ipversion:", ipver, "diff:", diff)
		}
		if diff.Changed[d1.Type()][0] != d6update {
			t.Error("Unexpected resource for ipversion:", ipver, "resource:", diff.New[d1.Type()][0])
		}
		if len(diff.New[d1.Type()]) != 0 {
			t.Error("Not empty new diff on add, ipversion:", ipver, "diff:", diff)
		}
		if len(diff.Gone[d1.Type()]) != 0 {
			t.Error("Not empty gone diff on add, ipversion:", ipver, "diff:", diff)
		}
	}
}

func TestRegisterGone(t *testing.T) {
	d1 := NewDummy(1, 1)
	d4 := NewDummy(2, 2)
	d4.IPVer = IPv4
	d6 := NewDummy(3, 3)
	d6.IPVer = IPv6

	c := NewBackendResources(&collectionConfig)
	c.Add(d1)
	c.Add(d4)
	c.Add(d6)

	chans := map[IPVersion]chan *ResourceDiff{}
	chans[IPAny] = make(chan *ResourceDiff, 3)
	chans[IPv4] = make(chan *ResourceDiff, 3)
	chans[IPv6] = make(chan *ResourceDiff, 3)
	for ipver, ch := range chans {
		c.RegisterChan(&ResourceRequest{
			RequestOrigin: partitionName,
			ResourceTypes: []string{d1.Type()},
			IPVersion:     ipver,
		}, ch)
	}

	// Expire the hashring nodes
	hashring := c.GetHashring(partitionName, d1.Type())
	for _, d := range []*Dummy{d1, d4, d6} {
		d.ExpiryTime = time.Minute * 10
		i, err := hashring.getIndex(d.Uid())
		if err != nil {
			t.Errorf("failed to retrieve existing resource: %s", err)
		}
		node := hashring.hashnodes[i]
		node.lastUpdate = time.Now().UTC().Add(-d.ExpiryTime - time.Minute)
	}

	pruned := c.Prune(d1.Type())
	if len(pruned) != 3 {
		t.Error("Unexpected number of pruned resources:", pruned)
	}

	for ipver, ch := range chans {
		chLen := 2
		if ipver == IPAny {
			chLen = 3
		}
		if len(ch) != chLen {
			t.Error(chLen, "updates expected after prune, got:", len(ch), "ipversion:", ipver)
			continue
		}
		close(ch)
		for diff := range ch {
			if len(diff.Gone[d1.Type()]) != 1 {
				t.Fatal("No resources in gone diff on prune, ipversion:", ipver, "diff:", diff)
			}
			if !ipver.ValidFor(diff.Gone[d1.Type()][0].IPVersion()) {
				t.Error("Unexpected resource for ipversion:", ipver, "resource:", diff.Gone[d1.Type()][0])
			}
			if len(diff.New[d1.Type()]) != 0 {
				t.Error("Not empty new diff on add, ipversion:", ipver, "diff:", diff)
			}
			if len(diff.Changed[d1.Type()]) != 0 {
				t.Error("Not empty changed diff on add, ipversion:", ipver, "diff:", diff)
			}

		}
	}
}

func TestRegisterUnpartitioned(t *testing.T) {
	d := NewDummy(1, 1)
	c := NewBackendResources(&CollectionConfig{
		Types: []TypeConfig{
			{Type: "dummy", Unpartitioned: true},
		},
	})

	ch := make(chan *ResourceDiff, 1)
	c.RegisterChan(&ResourceRequest{
		RequestOrigin: "unpartitionedDist",
		ResourceTypes: []string{d.Type()},
	}, ch)

	c.Add(d)
	if len(ch) != 1 {
		t.Fatal("Update expected after add")
	}

	diff := <-ch
	if len(diff.New[d.Type()]) != 1 {
		t.Fatal("Not resource in new diff on add diff:", diff)
	}
	if diff.New[d.Type()][0] != d {
		t.Error("Unexpected resource:", diff.New[d.Type()][0])
	}
}

func TestUnregister(t *testing.T) {
	d := NewDummy(1, 1)
	c := NewBackendResources(&collectionConfig)

	ch := make(chan *ResourceDiff, 1)
	c.RegisterChan(&ResourceRequest{
		RequestOrigin: partitionName,
		ResourceTypes: []string{d.Type()},
	}, ch)
	c.UnregisterChan(partitionName, ch)

	c.Add(d)
	if len(ch) != 0 {
		t.Error("Got update in an unregistered chan")
	}
}

func TestResourcesGet(t *testing.T) {
	d1 := NewDummy(1, 1)
	d4 := NewDummy(2, 2)
	d4.IPVer = IPv4
	d6 := NewDummy(3, 3)
	d6.IPVer = IPv6

	c := NewBackendResources(&collectionConfig)
	c.Add(d1)
	c.Add(d4)
	c.Add(d6)

	rs := c.Get(partitionName, d1.Type(), IPAny)
	if len(rs.Working) != 3 {
		t.Error("Unexpected number of working resources for IPAny:", len(rs.Working))
	}

	rs = c.Get(partitionName, d1.Type(), IPv4)
	if len(rs.Working) != 2 {
		t.Error("Unexpected number of working resources for IPv4:", len(rs.Working))
	}
	for _, r := range rs.Working {
		if r.IPVersion() == IPv6 {
			t.Error("Should not get IPv6 resources requesting IPv4:", r)
		}
	}

	rs = c.Get(partitionName, d1.Type(), IPv6)
	if len(rs.Working) != 2 {
		t.Error("Unexpected number of working resources for IPv6:", len(rs.Working))
	}
	for _, r := range rs.Working {
		if r.IPVersion() == IPv4 {
			t.Error("Should not get IPv4 resources requesting IPv6:", r)
		}
	}
}
