// Copyright (c) 2024, The Tor Project, Inc.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package common

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/ptutil/safeprom"
)

var (
	RdsysRequestsCount = safeprom.NewCounterVecRegistered(prometheus.CounterOpts{
		Name: "rdsys_request_total",
		Help: "The total number of bridge requests",
	},
		[]string{"distributor", "transport", "country", "lang", "provider", "ipv6"},
	)
)

func RdsysRequestsInc(labels prometheus.Labels) {
	finalLabels := prometheus.Labels{
		"distributor": "",
		"transport":   "",
		"country":     "",
		"lang":        "",
		"provider":    "",
		"ipv6":        "",
	}
	for name, value := range labels {
		finalLabels[name] = value
	}
	RdsysRequestsCount.With(finalLabels).Inc()
}
