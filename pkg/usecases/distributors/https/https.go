// Copyright (c) 2021-2022, The Tor Project, Inc.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package https

import (
	"fmt"
	"log"
	"net"
	"slices"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/internal"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/pkg/core"
	"gitlab.torproject.org/tpo/anti-censorship/rdsys/pkg/usecases/distributors/common"
)

const (
	DistName             = "https"
	BridgeReloadInterval = time.Minute * 10
)

// HttpsDistributor contains all the context that the distributor needs to run.
type HttpsDistributor struct {
	timeDistribution *common.TimeDistribution

	cfg *internal.Config
}

type RequestBridgesOptions struct {
	Type    string
	IP      net.IP
	IPv6    bool
	Country string
	Lang    string
}

// RequestBridges takes as tpe the type of the bridge requested,
// ip as the IP of the client, and ipv6 as whether IPv6 bridge is requested.
// and return a slice of bridge lines.
func (d *HttpsDistributor) RequestBridges(opts RequestBridgesOptions) ([]string, error) {
	if !slices.Contains(d.cfg.Distributors.Https.Resources, opts.Type) {
		return []string{}, fmt.Errorf("Unsuported transport type: %s", opts.Type)
	}

	common.RdsysRequestsInc(prometheus.Labels{
		"distributor": DistName,
		"transport":   opts.Type,
		"country":     opts.Country,
		"lang":        opts.Lang,
		"ipv6":        strconv.FormatBool(opts.IPv6),
	})
	req := &core.BridgeRequest{
		IPVersion: core.IPv4,
		Country:   opts.Country,
	}
	if opts.IPv6 {
		req.IPVersion = core.IPv6
	}
	r := d.timeDistribution.GetBridges(opts.Type, opts.IP, req)
	return r, nil
}

// Init initialises the given HTTPS distributor.
func (d *HttpsDistributor) Init(cfg *internal.Config) {
	log.Printf("Initialising %s distributor.", DistName)

	d.cfg = cfg
	log.Printf("Initialising resource stream.")
	d.timeDistribution = &common.TimeDistribution{
		ResourceStreamURL: cfg.Backend.ResourceStreamURL(),
		ApiToken:          cfg.Backend.ApiTokens[DistName],
		Resources:         d.cfg.Distributors.Https.Resources,
		IPVersion:         core.IPAny,
		DistName:          "https",
		Cfg:               &d.cfg.Distributors.Https.TimeDistribution,
	}
	d.timeDistribution.Start()
}

// Shutdown shuts down the given HTTPS distributor.
func (d *HttpsDistributor) Shutdown() {
	log.Printf("Shutting down %s distributor.", DistName)

	d.timeDistribution.Shutdown()
}
